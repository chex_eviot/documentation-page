import React from 'react';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';

function EviotAPIDocs() {
    return (
        <div>
            <h1 className='content-top-header'>Устройства и сеть </h1>
            <p className='content-text'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus
                accumsan et viverra justo commodo. Proin sodales pulvinar tempor.
                Cum sociis natoque penatibus et magnis dis parturient montes
                , nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                felis tellus mollis orci, sed rhoncus sapien nunc eget.</p>
            <div className="content-code">
                <h2 className="content-code-header">
                    Eviot.IsDeviceRegister
                </h2>
                <p className="content-code-text">
                    import    &#123;Meteor&#125; from 'meteor/meteor' (meteor/client_environment.js, line 31)
                </p>
            </div>
            <div className="content-code">
                <p className="content-code-text">
                    Boolean variable. True if running in client environment.
                </p>
            </div>
            <h2 className='content-bottom-header'>Инструменты</h2>
            <div className="navigation">
                <div className="left-navigation-block">
                    <ArrowBackIosIcon
                        style={{ color: "#292929" }}
                    />
                    <span>Предыдущее:</span> <a href="#">Название категории</a>
                </div>
                <div className="right-navigation-block">
                    <span>Следующее:</span><a href="#">Название категории</a>
                    <ArrowForwardIosIcon
                        style={{ color: "#292929" }}
                    />
                </div>

            </div>
        </div>
    )
        ;
}

export default EviotAPIDocs;
