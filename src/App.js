import React from 'react';
import './App.css';
import Accordeon from "./Accordeon"
import SearchIcon from '@material-ui/icons/Search';
import {Switch, Route, BrowserRouter} from 'react-router-dom'
import EviotAPIDocs from "./docs_pages/EviotAPIDocs";
import API from "./docs_pages/API";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";

function App() {
    return (
        <BrowserRouter>
            <div className="app">
                <div className="container my-container">
                    <div className="menu  ">
                        <h1 className="doc_header">Документация <sup className="doc_version">v1.0</sup></h1>
                        <div className="doc_search">
                            <span className="doc_search_icon"><SearchIcon
                                style={{ color: "#292929" }}
                            /></span>
                            <input type="text" className="form-control" placeholder="Поиск елементов"/>
                        </div>
                        <Accordeon/>
                    </div>
                    <div className="content">

                        <Switch>
                            <Route path="/" exact component={EviotAPIDocs}/>
                            <Route path="/api" component={API}/>
                        </Switch>
                    </div>

                </div>

            </div>
        </BrowserRouter>
    );
}

export default App;
