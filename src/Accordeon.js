import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import {Link as RouterLink} from 'react-router-dom';
import Link from '@material-ui/core/Link';

const useStyles = makeStyles(theme => ({

        root: {
            width: "100%",
            overflow: "auto",
            '&::-webkit-scrollbar': {
                width: '0.4em'
            },
            '&::-webkit-scrollbar-track': {
                boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
                webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)'
            },
            '&::-webkit-scrollbar-thumb': {
                backgroundColor: 'rgba(0,0,0,.1)',
                outline: '1px solid slategrey'
            },
        },
        heading: {
            fontSize: theme.typography.pxToRem(15),
            fontWeight: theme.typography.fontWeightRegular,
        },
        expanded: {
            '&$expanded': {
                margin: 0,
                'a': {
                    color: "blue",
                },
            },

            '& a': {
                color: 'blue',
            },

            fontSize: "50px",
        },

        header: {
            fontSize: " 20px",
            lineHeight: "24px",
            color: "#292929",
        },


    }))
;
export default function SimpleExpansionPanel() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <ExpansionPanel classes={
                {
                    expanded: classes.expanded,
                    root: classes.root,
                }
            }>
                <ExpansionPanelSummary
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <Typography className={classes.header}><Link component={RouterLink} to="/">Eviot API
                        Docs</Link></Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                        Устройства и сеть
                    </Typography>
                </ExpansionPanelDetails>
                <ExpansionPanelDetails>
                    <Typography>
                        Управление подключениями
                    </Typography>
                </ExpansionPanelDetails>
                <ExpansionPanelDetails>
                    <Typography>
                        Устройства и сеть
                    </Typography>
                </ExpansionPanelDetails>
                <ExpansionPanelDetails>
                    <Typography>
                        Управление Устройствами
                    </Typography>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel classes={{expanded: classes.expanded}}>
                <ExpansionPanelSummary

                    aria-controls="panel2a-content"
                    id="panel2a-header"
                >
                    <Typography className={classes.heading}> <Link component={RouterLink}
                                                                   to={"/api"}>API</Link></Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                        API point 1
                    </Typography>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel classes={{expanded: classes.expanded}}>
                <ExpansionPanelSummary

                    aria-controls="panel2a-content"
                    id="panel2a-header"
                >
                    <Typography className={classes.heading}> <Link to={"/core"}>Core</Link></Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                        Core point 1
                    </Typography>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel classes={{expanded: classes.expanded}}>
                <ExpansionPanelSummary

                    aria-controls="panel2a-content"
                    id="panel2a-header"
                >
                    <Typography className={classes.heading}>Less</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                        Less
                    </Typography>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel classes={{expanded: classes.expanded}}>
                <ExpansionPanelSummary

                    aria-controls="panel2a-content"
                    id="panel2a-header"
                >
                    <Typography className={classes.heading}>Webapp</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                        Less
                    </Typography>
                </ExpansionPanelDetails>
            </ExpansionPanel>


        </div>
    );
}